package com.example.eren.mattersframe.view.web;

import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.example.eren.mattersframe.utils.NetworkConnectionUtils;

/**
 * @author Eren
 */
public class MyWebViewClient extends WebViewClient {

    private IWebPageView mIWebPageView;

    private WebActivity mActivity;

    public MyWebViewClient(IWebPageView mIWebPageView) {
        this.mIWebPageView = mIWebPageView;
        mActivity = (WebActivity) mIWebPageView;
    }

    @Override
    public void onPageFinished(WebView view, String url) {
        if (!NetworkConnectionUtils.isConnected(mActivity)) {
            mIWebPageView.hindProgressBar();
        }
        super.onPageFinished(view, url);
    }
}
