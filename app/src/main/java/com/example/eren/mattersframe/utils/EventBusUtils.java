package com.example.eren.mattersframe.utils;


import android.app.usage.UsageEvents;

import org.greenrobot.eventbus.EventBus;

/**
 * @author Eren
 * <p>
 * 事件工具类
 */
public class EventBusUtils {

    /**
     * 注册事件
     */
    public static void register(Object subscriber) {
        EventBus.getDefault().register(subscriber);
    }

    /**
     * 解除事件
     */
    public static void unregister(Object subscriber) {
        EventBus.getDefault().unregister(subscriber);
    }

    /**
     * 发送普通事件
     */
    public static void sendEvent(UsageEvents.Event event) {
        EventBus.getDefault().post(event);
    }
}
