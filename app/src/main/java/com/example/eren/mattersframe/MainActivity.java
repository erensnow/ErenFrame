package com.example.eren.mattersframe;

import android.Manifest;
import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.MenuItem;
import android.widget.FrameLayout;

import com.example.eren.mattersframe.helper.BottomNavigationViewHelper;
import com.example.eren.mattersframe.ui.gankio.GankIoRootFragment;
import com.example.eren.mattersframe.ui.home.HomeRootFragment;
import com.example.eren.mattersframe.ui.movie.MovieRootFragment;
import com.example.eren.mattersframe.ui.welfare.WelfareRootFragment;
import com.tbruyelle.rxpermissions2.Permission;
import com.tbruyelle.rxpermissions2.RxPermissions;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.functions.Consumer;
import me.yokeyword.fragmentation.SupportActivity;
import me.yokeyword.fragmentation.SupportFragment;

/**
 * @author Eren
 * <p>
 * 主界面
 */
public class MainActivity extends SupportActivity {

    public static final int HOME = 0;
    public static final int GANKIO = 1;
    public static final int WELFARE = 2;
    public static final int MOVIE = 3;
    private static final String TAG = "MainActivity";

    @BindView(R.id.fl_container)
    FrameLayout flContainer;

    @BindView(R.id.bnv_bar)
    BottomNavigationView bottomNavigationView;

    @BindView(R.id.nav_view)
    NavigationView navigationView;

    @BindView(R.id.drawer_layout)
    DrawerLayout drawerLayout;

    private SupportFragment[] fragments = new SupportFragment[5];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        //权限申请
        requestPermissions();
        initFragment(savedInstanceState);
        initView();
    }

    /**
     * 权限申请
     */
    @SuppressLint("CheckResult")
    private void requestPermissions() {
        RxPermissions rxPermission = new RxPermissions(MainActivity.this);
        rxPermission
                .requestEach(
                        Manifest.permission.READ_PHONE_STATE)
                .subscribe(new Consumer<Permission>() {
                    @Override
                    public void accept(Permission permission) throws Exception {
                        if (permission.granted) {
                            // 用户已经同意该权限
                            Log.d(TAG, permission.name + " is granted.");
                        } else if (permission.shouldShowRequestPermissionRationale) {
                            // 用户拒绝了该权限，没有选中『不再询问』（Never ask again）,那么下次再次启动时，还会提示请求权限的对话框
                            Log.d(TAG, permission.name + " is denied. More info should be provided.");
                        } else {
                            // 用户拒绝了该权限，并且选中『不再询问』
                            Log.d(TAG, permission.name + " is denied.");
                        }
                    }
                });
    }

    /**
     * 初始化Fragment
     *
     * @param savedInstanceState
     */
    private void initFragment(Bundle savedInstanceState) {
        if (savedInstanceState == null) {
            fragments[HOME] = new HomeRootFragment();
            fragments[GANKIO] = new GankIoRootFragment();
            fragments[MOVIE] = new MovieRootFragment();
            fragments[WELFARE] = new WelfareRootFragment();

            loadMultipleRootFragment(R.id.fl_container, HOME,
                    fragments[HOME],
                    fragments[GANKIO],
                    fragments[MOVIE],
                    fragments[WELFARE]);
        } else {
            // 这里库已经做了Fragment恢复,所有不需要额外的处理了, 不会出现重叠问题
            // 这里我们需要拿到mFragments的引用,也可以通过getSupportFragmentManager.getFragments()
            // 自行进行判断查找(效率更高些),用下面的方法查找更方便些
            fragments[HOME] = findFragment(HomeRootFragment.class);
            fragments[GANKIO] = findFragment(GankIoRootFragment.class);
            fragments[MOVIE] = findFragment(MovieRootFragment.class);
            fragments[WELFARE] = findFragment(WelfareRootFragment.class);
        }
    }

    private void initView() {
        BottomNavigationViewHelper.disableShiftMode(bottomNavigationView);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.menu_item_home:
                        showHideFragment(fragments[HOME]);
                        break;
                    case R.id.menu_item_gank_io:
                        showHideFragment(fragments[GANKIO]);
                        break;
                    case R.id.menu_item_welfare:
                        showHideFragment(fragments[WELFARE]);
                        break;
                    case R.id.menu_item_movie:
                        showHideFragment(fragments[MOVIE]);
                        break;
                    default:
                        break;
                }
                return true;
            }
        });
    }
}
