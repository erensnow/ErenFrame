package com.example.eren.mattersframe.base;


import lombok.Getter;
import lombok.Setter;

/**
 * @author Eren
 * <p>
 * 基础response bean
 */
@Getter
@Setter
public class BaseResponseBean<T> {

    /**
     * 返回数据
     */
    private T data;
    /**
     * 0 代表执行成功  -1001 代表登录失效，需要重新登录
     */
    private int errorCode;
    /**
     * 返回信息
     */
    private String errorMsg;
}
