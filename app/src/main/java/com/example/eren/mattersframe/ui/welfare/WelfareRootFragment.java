package com.example.eren.mattersframe.ui.welfare;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.eren.mattersframe.R;

import me.yokeyword.fragmentation.SupportFragment;

/**
 * @author Eren
 */
public class WelfareRootFragment extends SupportFragment {
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable
            Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_welfare, container, false);
    }
}
