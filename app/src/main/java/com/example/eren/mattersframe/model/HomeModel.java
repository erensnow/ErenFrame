package com.example.eren.mattersframe.model;

import com.example.eren.mattersframe.base.BaseResponseBean;
import com.example.eren.mattersframe.bean.BannerBean;
import com.example.eren.mattersframe.contract.HomeContract;
import com.example.eren.mattersframe.http.RetrofitHttp;

import java.util.List;

import io.reactivex.Observable;

/**
 * @author Eren
 * <p>
 * 主页M层
 */
public class HomeModel implements HomeContract.Model {
    @Override
    public Observable<BaseResponseBean<List<BannerBean>>> getBannerList() {
        return RetrofitHttp.getInstance().getApiService().getBannerList();
    }
}
