package com.example.eren.mattersframe.presenter;

import com.example.eren.mattersframe.base.BaseResponseBean;
import com.example.eren.mattersframe.bean.BannerBean;
import com.example.eren.mattersframe.contract.HomeContract;

import java.util.List;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * @author Eren
 * <p>
 * 主页P层
 */
public class HomePresenter extends HomeContract.Presenter {

    @Override
    public void getBannerList() {
        mView.showLoading();
        mModel.getBannerList()
                //请求数据的事件发生在io线程
                .subscribeOn(Schedulers.io())
                //请求完成后在主线程更显UI
                .observeOn(AndroidSchedulers.mainThread())
                //订阅
                .subscribe(new Observer<BaseResponseBean<List<BannerBean>>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                    }

                    @Override
                    public void onNext(BaseResponseBean<List<BannerBean>> bannerBean) {
                        if (bannerBean != null) {
                            if (bannerBean.getErrorCode() == 0) {
                                mView.bannerList(bannerBean.getData());
                            } else {
                                mView.onError(bannerBean.getErrorMsg(), bannerBean.getErrorCode() + "");
                            }
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        mView.onError(e.getMessage(), "");
                    }

                    @Override
                    public void onComplete() {
                        mView.dismissLoading();
                    }
                });
    }
}
