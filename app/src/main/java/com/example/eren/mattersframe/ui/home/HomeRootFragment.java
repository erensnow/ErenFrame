package com.example.eren.mattersframe.ui.home;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.example.eren.mattersframe.R;
import com.example.eren.mattersframe.base.fragment.BaseMvpFragment;
import com.example.eren.mattersframe.bean.BannerBean;
import com.example.eren.mattersframe.contract.HomeContract;
import com.example.eren.mattersframe.model.HomeModel;
import com.example.eren.mattersframe.presenter.HomePresenter;
import com.example.eren.mattersframe.ui.home.child.TestActivity;
import com.youth.banner.Banner;
import com.youth.banner.loader.ImageLoader;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * @author Eren
 * <p>
 * 主页Fragment
 */
public class HomeRootFragment extends BaseMvpFragment<HomePresenter, HomeModel> implements HomeContract.View {

    @BindView(R.id.banner)
    Banner banner;

    List<BannerBean> listBanner = new ArrayList<>();

    @Override
    protected void initPresenter() {
        if (mPresenter != null) {
            mPresenter.setMV(mModel, this);
        }
    }

    @Override
    protected int setLayoutId() {
        return R.layout.fragment_home;
    }

    @Override
    protected void initView(Bundle savedInstanceState) {
        //获取Banner图
        mPresenter.getBannerList();
        banner.isAutoPlay(true);
        banner.setDelayTime(3000);
        banner.setImageLoader(new ImageLoader() {
            @Override
            public void displayImage(Context context, Object bean, ImageView imageView) {
                Glide.with(mContext).load(((BannerBean) bean).getImagePath()).into(imageView);
            }
        });
    }

    @Override
    public void bannerList(List<BannerBean> bannerList) {
        listBanner.clear();
        listBanner.addAll(bannerList);
        banner.setImages(listBanner);
        banner.start();
    }

    @OnClick(R.id.button)
    public void onViewClicked() {
        startActivity(new Intent(mContext, TestActivity.class));
    }
}
