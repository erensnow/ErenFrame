package com.example.eren.mattersframe.base.fragment;

import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.example.eren.mattersframe.R;
import com.example.eren.mattersframe.base.IBaseView;
import com.example.eren.mattersframe.base.bean.Event;
import com.example.eren.mattersframe.utils.EventBusUtils;
import com.example.eren.mattersframe.utils.PerfectClickListener;
import com.example.eren.mattersframe.utils.ResourcesUtils;
import com.example.eren.mattersframe.utils.ToastUtils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import me.yokeyword.fragmentation.SupportFragment;

/**
 * @author Eren
 * <p>
 * Fragment基类
 */
public abstract class BaseFragment extends SupportFragment implements IBaseView {

    /**
     * 主布局
     */
    public View rootView;
    /**
     * 上下文
     */
    public Context mContext;
    @BindView(R.id.img_error)
    ImageView ivError;
    @BindView(R.id.error_container)
    LinearLayout errorContainer;
    /**
     * 加载中
     */
    private View loadingView;
    /**
     * 数据绑定
     */
    private Unbinder unbinder;
    /**
     * 动画
     */
    private AnimationDrawable mAnimationDrawable;
    /**
     * 内容布局
     */
    private FrameLayout mContainer;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.mContext = context;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        // 初始化布局
        rootView = inflater.inflate(R.layout.fragment_base, container, false);
        mContainer = rootView.findViewById(R.id.container);
        mContainer.addView(getLayoutInflater().inflate(setLayoutId(), null));
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        mContainer.setLayoutParams(params);
        // 初始化ButterKnife
        unbinder = ButterKnife.bind(this, rootView);
        // 注册EventBus
        if (regEvent()) {
            EventBus.getDefault().register(this);
        }
        return rootView;
    }

    /**
     * 具体的布局
     *
     * @return 布局ID
     */
    protected abstract int setLayoutId();

    /**
     * 需要接收事件 重写该方法 并返回true
     */
    protected boolean regEvent() {
        return false;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        // 加载动画
        loadingView = ((ViewStub) getView(R.id.vs_loading)).inflate();
        ImageView img = loadingView.findViewById(R.id.img_progress);
        mAnimationDrawable = (AnimationDrawable) img.getDrawable();
        // 默认进入页面就开启动画
        if (!mAnimationDrawable.isRunning()) {
            mAnimationDrawable.start();
        }
        // 点击加载失败布局
        errorContainer.setOnClickListener(new PerfectClickListener() {
            @Override
            protected void onNoDoubleClick(View v) {
                showLoading();
                onRefresh();
            }
        });
        mContainer.setVisibility(View.GONE);
        // 隐藏加载
        dismissLoading();
        // 子类初始化布局
        initView(savedInstanceState);
    }

    protected View getView(int id) {
        if (getView() != null) {
            return getView().findViewById(id);
        }
        return null;
    }

    /**
     * 加载失败后点击后的操作
     */
    protected void onRefresh() {
    }

    /**
     * 初始化布局
     *
     * @param savedInstanceState 异常关闭数据保存
     */
    protected abstract void initView(Bundle savedInstanceState);

    @Override
    public void onDestroy() {
        super.onDestroy();
        // 注销ButterKnife
        if (unbinder != null) {
            unbinder.unbind();
        }
        // 注销EventBus
        if (regEvent()) {
            EventBusUtils.unregister(this);
        }
    }

    /**
     * 子类接受事件 重写该方法
     */
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventBus(Event event) {
    }

    @Override
    public void showToast(String msg) {
        ToastUtils.showToast(mContext, msg, 1000);
    }

    @Override
    public void showLoading() {
        // 显示加载
        if (loadingView != null && loadingView.getVisibility() != View.VISIBLE) {
            loadingView.setVisibility(View.VISIBLE);
        }
        // 开始动画
        if (!mAnimationDrawable.isRunning()) {
            mAnimationDrawable.start();
        }
        // 隐藏内容
        if (mContainer.getVisibility() != View.GONE) {
            mContainer.setVisibility(View.GONE);
        }
        // 隐藏错误信息
        if (errorContainer.getVisibility() != View.GONE) {
            errorContainer.setVisibility(View.GONE);
        }
    }

    @Override
    public void dismissLoading() {
        // 隐藏加载
        if (loadingView != null && loadingView.getVisibility() != View.GONE) {
            loadingView.setVisibility(View.GONE);
        }
        // 停止动画
        if (mAnimationDrawable.isRunning()) {
            mAnimationDrawable.stop();
        }
        // 显示内容
        if (mContainer.getVisibility() != View.VISIBLE) {
            mContainer.setVisibility(View.VISIBLE);
        }
        // 隐藏错误信息
        if (errorContainer.getVisibility() != View.GONE) {
            errorContainer.setVisibility(View.GONE);
        }
    }

    @Override
    public void onEmpty(Object tag) {
        // 隐藏加载
        if (loadingView != null && loadingView.getVisibility() != View.GONE) {
            loadingView.setVisibility(View.GONE);
        }
        // 停止动画
        if (mAnimationDrawable.isRunning()) {
            mAnimationDrawable.stop();
        }
        // 隐藏内容
        if (mContainer.getVisibility() != View.GONE) {
            mContainer.setVisibility(View.GONE);
        }
        // 显示错误信息
        if (errorContainer.getVisibility() != View.VISIBLE) {
            errorContainer.setVisibility(View.VISIBLE);
        }
        // 设置空显示图片
        ivError.setImageDrawable(ResourcesUtils.getDrawable(R.drawable.load_err));
    }

    @Override
    public void onError(Object tag, String errorMsg) {
        // 隐藏加载
        if (loadingView != null && loadingView.getVisibility() != View.GONE) {
            loadingView.setVisibility(View.GONE);
        }
        // 停止动画
        if (mAnimationDrawable.isRunning()) {
            mAnimationDrawable.stop();
        }
        // 隐藏内容
        if (mContainer.getVisibility() != View.GONE) {
            mContainer.setVisibility(View.GONE);
        }
        // 显示错误信息
        if (errorContainer.getVisibility() != View.VISIBLE) {
            errorContainer.setVisibility(View.VISIBLE);
        }
        // 设置错误显示图片
        ivError.setImageDrawable(ResourcesUtils.getDrawable(R.drawable.load_err));
    }

}
