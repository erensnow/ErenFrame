package com.example.eren.mattersframe.contract;

import com.example.eren.mattersframe.base.IBaseModel;
import com.example.eren.mattersframe.base.BasePresenter;
import com.example.eren.mattersframe.base.BaseResponseBean;
import com.example.eren.mattersframe.base.IBaseView;
import com.example.eren.mattersframe.bean.BannerBean;

import java.util.List;

import io.reactivex.Observable;


/**
 * 主界面接口
 *
 * @author Eren
 */
public interface HomeContract {
    interface View extends IBaseView {
        /**
         * Banner图
         *
         * @param bannerList
         */
        void bannerList(List<BannerBean> bannerList);
    }

    interface Model extends IBaseModel {
        /**
         * Banner图
         *
         * @return
         */
        Observable<BaseResponseBean<List<BannerBean>>> getBannerList();
    }

    abstract class Presenter extends BasePresenter<Model, View> {
        /**
         * Banner图
         */
        public abstract void getBannerList();
    }
}
