package com.example.eren.mattersframe.base.activity;

import android.content.Context;
import android.os.Bundle;

import com.example.eren.mattersframe.base.BasePresenter;
import com.example.eren.mattersframe.base.IBaseModel;
import com.example.eren.mattersframe.base.IBaseView;
import com.example.eren.mattersframe.utils.InstanceUtil;

import java.lang.reflect.ParameterizedType;

/**
 * @author Eren
 * <p>
 * Activity MVP基类
 */
public abstract class BaseMvpActivity<P extends BasePresenter, M extends IBaseModel> extends BaseActivity {

    public P mPresenter;
    public M mModel;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // 初始化Mvp
        initMvp();
    }

    /**
     * 初始化MVP
     */
    private void initMvp() {
        if (this instanceof IBaseView &&
                this.getClass().getGenericSuperclass() instanceof ParameterizedType &&
                ((ParameterizedType) (this.getClass().getGenericSuperclass())).getActualTypeArguments().length > 0) {
            mPresenter = InstanceUtil.getInstance(this, 0);
            mModel = InstanceUtil.getInstance(this, 1);
        }
        if (mPresenter != null) {
            mPresenter.setContext(this);
        }
        // 初始化Presenter
        if (mPresenter != null) {
            mPresenter.setMV(mModel, bindView());
        }
    }

    /**
     * 初始化Presenter
     *
     * @return
     */
    protected abstract Context bindView();

    @Override
    protected void onDestroy() {
        super.onDestroy();
        // 注销MVP
        detachMvp();
    }

    /**
     * 注销MVP
     */
    private void detachMvp() {
        if (mPresenter != null) {
            mPresenter.onDetached();
        }
    }
}
