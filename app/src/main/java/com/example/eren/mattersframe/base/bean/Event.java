package com.example.eren.mattersframe.base.bean;

import lombok.Getter;
import lombok.Setter;


/**
 * EventBus事件类
 *
 * @param <T>
 * @author Eren
 */
@Getter
@Setter
public class Event<T> {

    private String action;
    private T data;

    public Event(String action) {
        this.action = action;
    }

    public Event(String action, T data) {
        this.action = action;
        this.data = data;
    }
}
