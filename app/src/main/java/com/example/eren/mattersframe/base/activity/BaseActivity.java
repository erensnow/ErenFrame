package com.example.eren.mattersframe.base.activity;

import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.example.eren.mattersframe.R;
import com.example.eren.mattersframe.base.IBaseView;
import com.example.eren.mattersframe.base.bean.Event;
import com.example.eren.mattersframe.utils.ActivityCollector;
import com.example.eren.mattersframe.utils.EventBusUtils;
import com.example.eren.mattersframe.utils.PerfectClickListener;
import com.example.eren.mattersframe.utils.PermissionUtils;
import com.example.eren.mattersframe.utils.ResourcesUtils;
import com.example.eren.mattersframe.view.status.StatusBarUtil;
import com.orhanobut.logger.Logger;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import me.yokeyword.fragmentation.SupportActivity;
import me.yokeyword.fragmentation.anim.DefaultVerticalAnimator;
import me.yokeyword.fragmentation.anim.FragmentAnimator;

/**
 * @author Eren
 * <p>
 * Activity基类
 */
public abstract class BaseActivity extends SupportActivity implements IBaseView {

    @BindView(R.id.tool_bar)
    Toolbar toolBar;
    @BindView(R.id.img_error)
    ImageView ivError;
    @BindView(R.id.error_container)
    LinearLayout errorContainer;

    /**
     * 内容布局
     */
    private FrameLayout mContainer;
    /**
     * 加载中
     */
    private View loadingView;
    /**
     * 动画
     */
    private AnimationDrawable mAnimationDrawable;
    /**
     * 数据绑定
     */
    private Unbinder unbinder;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // 获取传递参数
        getParams(getIntent().getExtras());
        // 初始化布局
        initUI();
        // 注册EventBus
        if (regEvent()) {
            EventBusUtils.register(this);
        }
        // 当前所在类
        Logger.i("MainActivity", getClass().getSimpleName());
        // 将当前正在创建的活动添加到活动管理期里
        ActivityCollector.addActivity(this);
        // 初始化Toolbar
        setToolbar();
    }

    /**
     * 获取传递参数
     *
     * @param extras 传递参数
     */
    protected void getParams(Bundle extras) {
    }

    /**
     * 初始化布局
     */
    private void initUI() {
        setContentView(R.layout.activity_base);
        // 显示具体的布局界面，由子类显示
        mContainer = findViewById(R.id.container);
        mContainer.addView(View.inflate(this, setLayoutId(), null));
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        mContainer.setLayoutParams(params);
        // 初始化ButterKnife
        unbinder = ButterKnife.bind(this);
        // 设置透明状态栏，兼容4.4
        StatusBarUtil.setColor(this, ResourcesUtils.getColor(R.color.colorTheme), 0);
        // 加载动画
        loadingView = ((ViewStub) findViewById(R.id.vs_loading)).inflate();
        ImageView img = loadingView.findViewById(R.id.img_progress);
        mAnimationDrawable = (AnimationDrawable) img.getDrawable();
        // 默认进入页面就开启动画
        if (!mAnimationDrawable.isRunning()) {
            mAnimationDrawable.start();
        }
        // 点击加载失败布局
        errorContainer.setOnClickListener(new PerfectClickListener() {
            @Override
            protected void onNoDoubleClick(View v) {
                showLoading();
                onRefresh();
            }
        });
        mContainer.setVisibility(View.GONE);
    }

    /**
     * 需要接收事件 重写该方法 并返回true
     */
    protected boolean regEvent() {
        return false;
    }

    /**
     * 对Toolbar进行设置
     */
    private void setToolbar() {
        setSupportActionBar(toolBar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            // 去除默认Title显示
            actionBar.setDisplayShowTitleEnabled(false);
            actionBar.setDisplayHomeAsUpEnabled(true);
            // 设置返回键图片
            actionBar.setHomeAsUpIndicator(R.drawable.icon_back);
        }
        // 返回键的点击事件
        toolBar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    finishAfterTransition();
                } else {
                    onBackPressed();
                }
            }
        });
    }

    /**
     * 具体的布局
     *
     * @return 布局ID
     */
    protected abstract int setLayoutId();

    /**
     * 失败后点击刷新
     */
    protected void onRefresh() {
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        // 注销ButterKnife
        if (unbinder != null) {
            unbinder.unbind();
        }
        // 注销EventBus
        if (regEvent()) {
            EventBusUtils.unregister(this);
        }
        // 将一个马上要销毁的活动从管理器里移除
        ActivityCollector.removeActivity(this);
    }

    @Override
    public FragmentAnimator onCreateFragmentAnimator() {
        //fragment切换使用默认Vertical动画
        return new DefaultVerticalAnimator();
    }

    @Override
    protected void onStart() {
        super.onStart();
        initView();
    }

    /**
     * 初始化布局
     */
    protected abstract void initView();

    @Override
    public void setTitle(CharSequence title) {
        toolBar.setTitle(title);
    }

    /**
     * 子类接受事件 重写该方法
     */
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventBus(Event event) {
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        PermissionUtils.onRequestPermissionsResult(requestCode, permissions, grantResults);
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    /**
     * 页面跳转
     *
     * @param clz 要跳转的Activity
     */
    public void startActivity(Class<?> clz) {
        startActivity(new Intent(this, clz));
    }

    /**
     * 页面跳转
     *
     * @param clz    要跳转的Activity
     * @param intent intent
     */
    public void startActivity(Class<?> clz, Intent intent) {
        intent.setClass(this, clz);
        startActivity(intent);
    }

    /**
     * 携带数据的页面跳转
     *
     * @param clz    要跳转的Activity
     * @param bundle bundle数据
     */
    public void startActivity(Class<?> clz, Bundle bundle) {
        Intent intent = new Intent();
        intent.setClass(this, clz);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        startActivity(intent);
    }

    /**
     * 含有Bundle通过Class打开编辑界面
     *
     * @param clz         要跳转的Activity
     * @param bundle      bundle数据
     * @param requestCode requestCode
     */
    public void startActivityForResult(Class<?> clz, Bundle bundle,
                                       int requestCode) {
        Intent intent = new Intent();
        intent.setClass(this, clz);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        startActivityForResult(intent, requestCode);
    }

    @Override
    public void showToast(String msg) {
    }

    @Override
    public void showLoading() {
        // 显示加载
        if (loadingView != null && loadingView.getVisibility() != View.VISIBLE) {
            loadingView.setVisibility(View.VISIBLE);
        }
        // 开始动画
        if (!mAnimationDrawable.isRunning()) {
            mAnimationDrawable.start();
        }
        // 隐藏内容
        if (mContainer.getVisibility() != View.GONE) {
            mContainer.setVisibility(View.GONE);
        }
        // 隐藏错误信息
        if (errorContainer.getVisibility() != View.GONE) {
            errorContainer.setVisibility(View.GONE);
        }
    }

    @Override
    public void dismissLoading() {
        // 隐藏加载
        if (loadingView != null && loadingView.getVisibility() != View.GONE) {
            loadingView.setVisibility(View.GONE);
        }
        // 停止动画
        if (mAnimationDrawable.isRunning()) {
            mAnimationDrawable.stop();
        }
        // 显示内容
        if (mContainer.getVisibility() != View.VISIBLE) {
            mContainer.setVisibility(View.VISIBLE);
        }
        // 隐藏错误信息
        if (errorContainer.getVisibility() != View.GONE) {
            errorContainer.setVisibility(View.GONE);
        }
    }

    @Override
    public void onEmpty(Object tag) {
        // 隐藏加载
        if (loadingView != null && loadingView.getVisibility() != View.GONE) {
            loadingView.setVisibility(View.GONE);
        }
        // 停止动画
        if (mAnimationDrawable.isRunning()) {
            mAnimationDrawable.stop();
        }
        // 隐藏内容
        if (mContainer.getVisibility() != View.GONE) {
            mContainer.setVisibility(View.GONE);
        }
        // 显示错误信息
        if (errorContainer.getVisibility() != View.VISIBLE) {
            errorContainer.setVisibility(View.VISIBLE);
        }
        // 设置空显示图片
        ivError.setImageDrawable(ResourcesUtils.getDrawable(R.drawable.load_err));
    }

    @Override
    public void onError(Object tag, String errorMsg) {
        // 隐藏加载
        if (loadingView != null && loadingView.getVisibility() != View.GONE) {
            loadingView.setVisibility(View.GONE);
        }
        // 停止动画
        if (mAnimationDrawable.isRunning()) {
            mAnimationDrawable.stop();
        }
        // 隐藏内容
        if (mContainer.getVisibility() != View.GONE) {
            mContainer.setVisibility(View.GONE);
        }
        // 显示错误信息
        if (errorContainer.getVisibility() != View.VISIBLE) {
            errorContainer.setVisibility(View.VISIBLE);
        }
        // 设置错误显示图片
        ivError.setImageDrawable(ResourcesUtils.getDrawable(R.drawable.load_err));
    }
}
