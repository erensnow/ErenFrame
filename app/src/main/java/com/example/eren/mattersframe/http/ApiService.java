package com.example.eren.mattersframe.http;

import com.example.eren.mattersframe.base.BaseResponseBean;
import com.example.eren.mattersframe.bean.BannerBean;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.GET;


/**
 * @author Eren
 * <p>
 * Api接口
 */
public interface ApiService {
    /**
     * 首页
     *
     * @return
     */
    @GET("banner/json")
    Observable<BaseResponseBean<List<BannerBean>>> getBannerList();
}
