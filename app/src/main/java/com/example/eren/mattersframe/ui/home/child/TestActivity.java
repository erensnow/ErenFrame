package com.example.eren.mattersframe.ui.home.child;

import android.content.Context;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.example.eren.mattersframe.R;
import com.example.eren.mattersframe.base.activity.BaseMvpActivity;
import com.example.eren.mattersframe.bean.BannerBean;
import com.example.eren.mattersframe.contract.HomeContract;
import com.example.eren.mattersframe.model.HomeModel;
import com.example.eren.mattersframe.presenter.HomePresenter;
import com.youth.banner.Banner;
import com.youth.banner.loader.ImageLoader;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * @author Eren
 * <p>
 * Main案例
 */
public class TestActivity extends BaseMvpActivity<HomePresenter, HomeModel> implements HomeContract.View {

    @BindView(R.id.banner)
    Banner banner;

    List<BannerBean> listBanner = new ArrayList<>();

    @Override
    protected int setLayoutId() {
        return R.layout.activity_second;
    }

    @Override
    protected void initView() {
        //获取Banner图
        mPresenter.getBannerList();
        setTitle("测试Activity");
        banner.isAutoPlay(true);
        banner.setDelayTime(3000);
        banner.setImageLoader(new ImageLoader() {
            @Override
            public void displayImage(Context context, Object bean, ImageView imageView) {
                Glide.with(TestActivity.this).load(((BannerBean) bean).getImagePath()).into(imageView);
            }
        });
    }

    @Override
    protected Context bindView() {
        return TestActivity.this;
    }

    @Override
    public void bannerList(List<BannerBean> bannerList) {
        listBanner.clear();
        listBanner.addAll(bannerList);
        banner.setImages(listBanner);
        banner.start();
    }
}
