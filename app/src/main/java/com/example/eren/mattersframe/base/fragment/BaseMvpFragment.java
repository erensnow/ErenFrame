package com.example.eren.mattersframe.base.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;

import com.example.eren.mattersframe.base.BasePresenter;
import com.example.eren.mattersframe.base.IBaseModel;
import com.example.eren.mattersframe.base.IBaseView;
import com.example.eren.mattersframe.utils.InstanceUtil;

import java.lang.reflect.ParameterizedType;

/**
 * @author Eren
 * <p>
 * Fragment MVP基类
 */
public abstract class BaseMvpFragment<P extends BasePresenter, M extends IBaseModel> extends BaseFragment {

    public P mPresenter;
    public M mModel;

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        // 初始化Mvp
        initMvp();
    }

    /**
     * 初始化P、M
     */
    private void initMvp() {
        if (this instanceof IBaseView &&
                this.getClass().getGenericSuperclass() instanceof ParameterizedType &&
                ((ParameterizedType) (this.getClass().getGenericSuperclass())).getActualTypeArguments().length > 0) {
            mPresenter = InstanceUtil.getInstance(this, 0);
            mModel = InstanceUtil.getInstance(this, 1);
        }
        if (mPresenter != null) {
            mPresenter.setContext(mContext);
        }
        // 初始化Presenter
        initPresenter();
    }

    /**
     * 初始化Presenter
     */
    protected abstract void initPresenter();

    @Override
    public void onDestroy() {
        super.onDestroy();
        // 注销MVP
        detachMvp();
    }

    /**
     * 注销MVP
     */
    private void detachMvp() {
        if (mPresenter != null) {
            mPresenter.onDetached();
        }
    }
}
